@extends('layouts.dashboard')

@section('content')

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Daftar Event Cagar Budaya</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Event</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->

<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Tabel Event Cagar Budaya</h3>
                </div>
                <div class="card-body">
                    <a href="/admin/eventcagarbudaya/add" class="btn btn-secondary">Tambah Event</a>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Nama Acara</th>
                                <th>Cagar Budaya</th>
                                <th>Tanggal</th>
                                <th>Waktu</th>
                                <th>Deskripsi</th>

                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($events as $ev)
                            <tr>
                                <td>
                                    {{$ev->nama}}
                                </td>
                                <td>
                                    @foreach($ev->cagarbudayas as $cagarbudaya)
                                    {{$cagarbudaya->nama}} <br>
                                    @endforeach
                                </td>
                                <td>
                                    {{$ev->tanggal_mulai}} s/d {{$ev->tanggal_selesai}}
                                </td>
                                <td>
                                    {{$ev->waktu_mulai}} <br> {{$ev->waktu_selelai}}
                                </td>
                                <td>
                                    {{$ev->deskripsi}}
                                </td>

                                <td>
                                    <a class="btn btn-danger" href="/admin/eventcagarbudaya/hapus/{id}">Hapus</a>
                                    <a class="btn btn-secondary" href="/admin/eventcagarbudaya/edit/{id}">Edit</a></td>
                                </td>

                            </tr>

                            @endforeach
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->


        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

@endsection