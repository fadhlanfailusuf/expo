@extends('layouts.dashboard')

@section('content')

<div class="card uper">
    <div class="card-header">
        Form Tambah Event Cagar Budaya
    </div>
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div> <br />
        @endif
        <form action="/admin/eventcagarbudaya/store" method="post">
            @csrf
            <div class="form-group">
                <label>Nama Acara</label>
                <input type="text" class="form-control" required="required" name="nama">
                <label>Pilih Cagar Budaya</label>
                <select class="form-control" name="cagarbudaya_id" data-live-search="true" style="width:100%">
                    <option value=""> --Silahkan Pilih-- </option>
                    @foreach ($cagarbudayas as $cat)
                    <option value="{{ $cat->id }}">{{ $cat->nama }}</option>
                    @endforeach
                </select>
                <label>Tanggal Mulai</label>
                <input type="date" class="form-control" required="required" name="tanggal_mulai">
                <label>Tanggal Selesai</label>
                <input type="date" class="form-control" required="required" name="tanggal_selesai">
                <label>Waktu Mulai</label>
                <input type="time" class="form-control" required="required" name="waktu_mulai">
                <label>Waktu Selesai</label>
                <input type="time" class="form-control" required="required" name="waktu_selesai">
                <label>Deskripsi</label>
                <textarea name="deskripsi" cols="130" rows="4"></textarea>
                                
            </div>
            <button type="submit" class="btn btn-primary">Tambah Event Cagar Budaya</button>
        </form>
    </div>
</div>
@endsection