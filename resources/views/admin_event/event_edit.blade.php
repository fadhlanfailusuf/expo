@extends('layouts.dashboard')

@section('content')

<div class="card uper">
    <div class="card-header">
        Form Edit Cagar Budaya
    </div>
    <div class="card-body">
        @foreach($events as $ev)
        <form action="/admin/eventcagarbudaya/update" method="post">
            @csrf
            <div class="form-group">
                <input type="hidden" name="id" value="{{$ev->id}}"> <br/>
                <label>Nama Acara</label>
                <input type="text" class="form-control" required='required' name="nama" value="{{ $ev->nama }}"> <br>
                <label>Pilih Cagar Budaya</label>
                <select class="form-control" name="cagarbudaya_id" data-live-search="true" style="width:100%">
                    <option value="{{ $ev->cagarbudaya_id }}"> --Silahkan Pilih-- </option>
                    @foreach ($cagarbudayas as $cat)
                    <option value="{{ $cat->id }}">{{ $cat->nama }}</option>
                    @endforeach
                </select>
                <label>Tanggal Mulai</label>
                <input type="date" class="form-control" required="required" name="tanggal_mulai" value="{{ $ev->tanggal_mulai }}">
                <label>Tanggal Selesai</label>
                <input type="date" class="form-control" required="required" name="tanggal_selesai" value="{{ $ev->tanggal_selesai }}">
                <label>Waktu Mulai</label>
                <input type="time" class="form-control" required="required" name="waktu_mulai" value="{{ $ev->waktu_mulai }}">
                <label>Waktu Selesai</label>
                <input type="time" class="form-control" required="required" name="waktu_selesai" value="{{ $ev->waktu_selesai }}">
                <label>Deskripsi</label>
                <textarea name="deskripsi" cols="130" rows="4" value="{{ $ev->deskripsi }}"></textarea>
            
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
        @endforeach
    </div>
</div>

@endsection