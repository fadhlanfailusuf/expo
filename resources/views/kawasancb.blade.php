<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="http://maps.googleapis.com/maps/api/js"></script>

    <title>Cubic | Percaya</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">


</head>

<body>

    <!-- Navbar (sit on top) -->
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
            <img src="{{ asset('images/logo.jpg') }}" height="50" width="50">
                <a href="/" class="w3-bar-item w3-button"><b>Cubic</b> Percaya</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        <li class="nav-item">
                            <a class="nav-link" href="/cagarbudaya">Jenis</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/buatlaporan">Lapor</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/event">Event</a>
                        </li>
                        @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                        @endif
                        @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
    </div>

    <div class="container">
        <h3><center>Kawasan cagar budaya di Yogyakarta</center></h3>
        <!-- <a href="#">1. Kotagede</a> <br>
    <a href="#">2. Keraton</a> <br>
    <a href="#">3. Malioboro</a> <br>
    <a href="#">4. Pakualaman</a> <br>
    <a href="#">5. Kotabaru</a> <br>
    <a href="#">6. Imagiri</a> <br> -->

        <div class="card" style="width: 50rem position: center;">
            <img src="..." class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">Kotagede</h5>
                <p class="card-text">Deskripsi</p>
                <a href="#" class="btn btn-primary">Lihat</a>
            </div>
        </div>
        <br>

        <div class="card" style="width: 50rem position: center;">
            <img src="..." class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">Keraton</h5>
                <p class="card-text">Deskripsi</p>
                <a href="#" class="btn btn-primary">Lihat</a>
            </div>
        </div>
        <br>

        <div class="card" style="width: 50rem position: center;;">
            <img src="..." class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">Malioboro</h5>
                <p class="card-text">Deskripsi</p>
                <a href="#" class="btn btn-primary">Lihat</a>
            </div>
        </div>
        <br>

        <div class="card" style="width: 50rem position: center;">
            <img src="..." class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">Kotabaru</h5>
                <p class="card-text">Deskripsi</p>
                <a href="#" class="btn btn-primary">Lihat</a>
            </div>
        </div>
        <br>

        <div class="card" style="width: 50rem position: center;">
            <img src="..." class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">Pakualaman</h5>
                <p class="card-text">Deskripsi</p>
                <a href="#" class="btn btn-primary">Lihat</a>
            </div>
        </div>
        <br>

        <div class="card" style="width: 50rem position: center;">
            <img src="..." class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">Imagiri</h5>
                <p class="card-text">Deskripsi</p>
                <a href="#" class="btn btn-primary">Lihat</a>
            </div>
        </div>


    </div>


  
    <!-- Footer
    <footer class="w3-center w3-black w3-padding-16">
        <p>Powered by <a href="https://www.w3schools.com/w3css/default.asp" title="W3.CSS" target="_blank" class="w3-hover-text-green">w3.css</a></p>
    </footer> -->


</body>

</html>