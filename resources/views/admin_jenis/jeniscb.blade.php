@extends('layouts.dashboard')

@section('content')

<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Daftar Kategori Cagar Budaya</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Kategori Cagar Budaya</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->

<section class="content">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Tabel Daftar Cagar Budaya</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <a href="/admin/jeniscagarbudaya/add" class="btn btn-secondary">Tambah Kategori Cagar Budaya</a>
        </div>
        <div class="card-body">
        <table class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Nama</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              @foreach($jeniscbs as $cat)
              <tr>
                <td>
                  {{$cat->nama}}
                </td>
                <td>
                  <a class="btn btn-danger" href="/admin/jeniscagarbudaya/hapus/{{$cat->id}}">Hapus</a>
                  <a class="btn btn-secondary" href="/admin/jeniscagarbudaya/edit/{{$cat->id}}">Edit</a>
                </td>

              </tr>

              @endforeach
            </tbody>

          </table>
        </div>
        <!-- /.card-body -->
        
      </div>
      <!-- /.card -->


    </div>
    <!-- /.card -->
  </div>
  <!-- /.col -->
  </div>
  <!-- /.row -->
</section>

@endsection