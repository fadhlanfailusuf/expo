@extends('layouts.dashboard')

@section('content')

<div class="card uper">
    <div class="card-header">
        Form Tambah Jenis Cagar Budaya
    </div>
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div> <br/>
        @endif
        <form action="/admin/jeniscagarbudaya/store" method="post">
            @csrf
            <div class="form-group">
                <label>Nama Kategori</label>
                <input type="text" class="form-control" name="nama" required="required">
            </div>
            <button type="submit" class="btn btn-primary">Tambah Kategori</button>
        </form>
    </div>
</div>
@endsection