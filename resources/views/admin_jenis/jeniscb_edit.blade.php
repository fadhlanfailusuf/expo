@extends('layouts.dashboard')

@section('content')

<div class="card uper">
    <div class="card-header">
        Form Edit Kategori Cagar Budaya
    </div>
    <div class="card-body">
        @foreach($categories as $cat)
        <form action="/admin/jeniscagarbudaya/update" method="post">
            @csrf
            <div class="form-group">
                <input type="hidden" name="id" value="{{$cat->id}}"> <br/>
                <label>Nama Kategori</label>
                <input type="text" class="form-control" required='required' name="nama" value="{{ $cat->nama }}">
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
        @endforeach
    </div>
</div>

@endsection