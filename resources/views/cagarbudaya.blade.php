@extends('layouts.nav')

@section('content')
<section>
    <div class="w3-content">
        <div class="card">
            <div class="container">
                <div class="card-header">
                    <h4>Daftar Cagar Budaya</h4>
                </div>
                <div class="row">
                @foreach($cagarbudayas as $cb)
                    <div class="col-md-4">
                        <div class="event_post">
                            <img src="image/blog1.jpg" alt="">
                            <a href="#"><h2 class="event_title">{{$cb->nama}}</h2></a>
                            <ul class="list_style sermons_category event_category">
                                <li><i class="lnr lnr-user"></i>{{$cb->jeniscb->nama}}</li>
                            </ul>
                            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" data-id="'.$row['ID'].'">Lihat Detail</button>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>

        @foreach($cagarbudayas as $cb)
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- konten modal-->
                <div class="modal-content">
                    <!-- heading modal -->
                    <div class="modal-header">
                        <h4 class="modal-title">{{$cb->nama}}</h4>
                    </div>
                    <!-- body modal -->
                    <div class="modal-body">
                        <table class="table table-striped table-responsive-md">
                            <tr>
                                <th>Jenis Cagar Budaya</th>
                                <td>:</td>
                                <td>{{$cb->jeniscb->nama}}</td>
                            </tr>
                            <tr>
                                <th>Alamat</th>
                                <td>:</td>
                                <td>{{$cb->alamat}}</td>
                            </tr>
                            <tr>
                                <th>Kota</th>
                                <td>:</td>
                                <td>{{$cb->kota}}</td>
                            </tr>
                            <tr>
                                <th>Deskripsi</th>
                                <td>:</td>
                                <td>{{$cb->deskripsi}}</td>
                            </tr>
                        </table>
                    </div>
                    <!-- footer modal -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>

</section>
@endsection