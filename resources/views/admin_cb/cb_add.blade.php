@extends('layouts.dashboard')

@section('content')

<div class="card uper">
    <div class="card-header">
        Form Tambah Cagar Budaya
    </div>
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div> <br />
        @endif
        <form action="/admin/cagarbudaya/store" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label>Nama Cagar Budaya</label>
                <input type="text" class="form-control" required="required" name="nama">
                <label>Alamat</label>
                <input type="text" class="form-control" required="required" name="alamat">
                <label>Kategori Cagar Budaya</label>
                <select class="form-control" name="jeniscb_id" data-live-search="true" style="width:100%">
                    <option> --Silahkan Pilih-- </option>
                    @foreach ($jeniscbs as $cat)
                    <option value="{{ $cat->id }}">{{ $cat->nama }}</option>
                    @endforeach
                </select>
                <label>Kota</label>
                <input type="text" class="form-control" required='required' name="kota">
                <label>Titik Long Location</label>
                <input type="text" class="form-control" name="long_location">
                <label>Titik Long Location</label>
                <input type="text" class="form-control"  name="lat_location"> <br>
                <label>Deskripsi</label>
                <textarea name="deskripsi" cols="130" rows="4"></textarea>
                <label>Gambar Cagar Budaya</label> <br>
                <input type="file" name="gambar">
                
                
            </div>
            <button type="submit" class="btn btn-primary">Tambah Cagar Budaya</button>
        </form>
    </div>
</div>
@endsection