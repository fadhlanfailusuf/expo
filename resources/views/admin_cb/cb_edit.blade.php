@extends('layouts.dashboard')

@section('content')

<div class="card uper">
    <div class="card-header">
        Form Edit Cagar Budaya
    </div>
    <div class="card-body">
        @foreach($cagarbudayas as $cb)
        <form action="/admin/cagarbudaya/update" method="post">
            @csrf
            <div class="form-group">
                <input type="hidden" name="id" value="{{$cb->id}}"> <br/>
                <label>Nama Cagar Budaya</label>
                <input type="text" class="form-control" required="required" name="nama" value="{{$cb->nama}}">
                <label>Alamat</label>
                <input type="text" class="form-control" required="required" name="alamat" value="{{$cb->alamat}}">
                <label>Kategori Cagar Budaya</label>
                <select class="form-control" name="jeniscb_id" data-live-search="true" style="width:100%" value="{{$cb->jeniscb_id}}">
                    <option> --Silahkan Pilih-- </option>
                    @foreach ($jeniscbs as $cat)
                    <option value="{{ $cat->id }}">{{ $cat->nama }}</option>
                    @endforeach
                </select>
                <label>Kota</label>
                <input type="text" class="form-control" required='required' name="kota" value="{{$cb->kota}}">
                <label>Titik Long Location</label>
                <input type="text" class="form-control" name="long_location" value="{{$cb->long_location}}">
                <label>Titik Long Location</label>
                <input type="text" class="form-control"  name="lat_location" value="{{$cb->lat_location}}"> <br>
                <label>Deskripsi</label>
                <textarea name="deskripsi" cols="130" rows="4" value="{{$cb->deskripsi}}"></textarea>
                <label>Gambar Cagar Budaya</label> <br>
                <input type="file" name="gambar" value="{{$cb->gambar}}">
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
        @endforeach
    </div>
</div>

@endsection