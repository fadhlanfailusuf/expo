@extends('layouts.dashboard')

@section('content')

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Daftar Cagar Budaya</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item active">Daftar Cagar Budaya</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->

<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Tabel Daftar Cagar Budaya</h3>
                </div>
                <div class="card-body">
                    <a href="/admin/cagarbudaya/add" class="btn btn-secondary">Tambah Cagar Budaya</a>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nama</th>
                                <th>Jenis Cagar Budaya</th>
                                <th>Alamat</th>
                                <th>Kota</th>
                                <th>Deskripsi</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($cagarbudayas as $cb)
                            <tr>
                                <td>
                                    {{$cb->id}}
                                </td>
                                <td>
                                    {{$cb->nama}}
                                </td>
                                <td>
                                    {{$cb->jeniscb->nama}}
                                </td>
                                <td>
                                    {{$cb->alamat}}
                                </td>
                                <td>
                                    {{$cb->kota}}
                                </td>
                                <td>
                                    {{$cb->deskripsi}}
                                </td>
                                <td>
                                    <a class="btn btn-danger" href="/admin/cagarbudaya/hapus/{{$cb->id}}">Hapus</a>
                                    <a class="btn btn-secondary" href="/admin/cagarbudaya/edit/{{$cb->id}}">Edit</a></td>
                                </td>

                            </tr>

                            @endforeach
                        </tbody>

                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->


        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

@endsection