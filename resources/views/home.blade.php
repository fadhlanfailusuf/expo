@extends('layouts.nav')

@section('content')
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div> -->

<!--================banner Area =================-->
<section class="banner_area d-flex text-center">
  <div class="container align-self-center">
    <div class="row">
      <div class="col-md-12">
        <div class="banner_content">
          <h6>Selamat Datang Di</h6>
          <h1>PERCAYA</h1>
          <h4>Kunjungi, Lindungi, Lestarikan</h4>
          <!-- <a href="#" class="btn_hover btn_hover_two">Mulai</a> -->
        </div>
      </div>
    </div>
  </div>
</section>
<!--================banner Area =================-->

<!--================About Area =================-->
<section class="about_area section_gap">
  <div class="container">
    <div class="section_title text-center">
      <h2>Pelestarian Cagar Budaya</h2>
      <h3>Percaya</h3>
    </div>
    <div class="row">
      <div class="col-md-6 d_flex">
        <div class="about_content flex">
          <h3 class="title_color">Apa itu Percaya ?</h3>
          <p>Percaya adalah sebuah web yang memberikan informasi seputar Cagar Budaya yang ada di Yogyakarta, <br> dan juga memberikan informasi mengenai acara yang diselengarakan di Cagar Budaya <br> Selain itu kalian bisa melaporkan kerusakan yang ada di Cagar Budaya tersebut.</p>
          <!-- <a href="#" class="about_btn btn_hover">Read Full Story</a> -->
        </div>
      </div>
      <div class="col-md-6">
        <img src="image/tentang.jpg" alt="abou_img">
      </div>
    </div>
  </div>
</section>
<!--================About Area =================-->


<!--================Project Section =================-->
<!-- <section class="event_blog_area section_gap">
  <div class="container">
    <div class="section_title text-center">
      <h2>Peta Cagar Budaya</h2>
    </div>
    <div class="container" id="googleMap" style="width:100%;height:380px;">
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </div>
  </div>
</section> -->



<!--================Project Section =================-->




<!--================Features Area =================-->
<!-- <section class="features_area">
  <div class="row m0">
    @foreach($cagarbudayas as $cb)
    <div class="col-md-3 features_item">
      <h3>{{$cb->nama}}</h3>
      <p>{{$cb->alamat}}</p>
      <a href="#" class="btn_hover view_btn">View Details</a>
    </div>
    @endforeach
  </div>
</section> -->
<!--================Features Area =================-->


<!--================Event Blog Area=================-->
<!-- <section class="event_blog_area section_gap">
  <div class="container">
    <div class="section_title text-center">
      <h2>Cagar Budaya</h2>
    </div>
    <div class="row">
      @foreach($cagarbudayas as $cb)
      <div class="col-md-4">
        <div class="event_post">
          <img src="image/prambanan.jpg" alt="">
          <a href="#">
            <h2 class="event_title">{{$cb->nama}}</h2>
          </a>
          <ul class="list_style sermons_category event_category">
            <li><i class="lnr lnr-location"></i>{{$cb->kota}}</li>
            <li><i class="lnr lnr-location"></i>{{$cb->lokasi}}</li>
          </ul>
          <a href="/cagarbudaya" class="btn_hover">Lihat Lebih Banyak</a>
        </div>
      </div>
      @endforeach
    </div>
  </div>
  </div>
</section> -->
<!--================Blog Area=================-->



<!-- About Section -->
<section class="event_blog_area section_gap">
  <div class="container">
    <div class="section_title text-center">
      <h2>Anggota Tim</h2>
    </div>
    <div class="row">
      <div class="col-md-3">
        <div class="event_post">
          <img src="{{ asset('images/fadhlan.jpeg') }}" alt="Fadhlan" style="width:50%" class="rotateimg180">
          <h3>Fadhlan Failusuf S</h3>
          <p class="w3-opacity">18523142</p>
          <p>Job Desc</p>
        </div>
      </div>
      <div class="col-md-3">
        <div class="event_post">
          <img src="{{ asset('images/#.jpeg') }}" alt="Fadhlan" style="width:100%" class="rotateimg180">
          <h3>M Fajri Ashshiddiq</h3>
          <p class="w3-opacity">18523263</p>
          <p>Job Desc</p>
        </div>
      </div>
      <div class="col-md-3">
        <div class="event_post">
          <img src="{{ asset('images/#.jpeg') }}" alt="Fadhlan" style="width:100%" class="rotateimg180">
          <h3>Zetram Adhitya</h3>
          <p class="w3-opacity">18523143</p>
          <p>Job Desc</p>
        </div>
      </div>
      <div class="col-md-3">
        <div class="event_post">
          <img src="{{ asset('images/#.jpeg') }}" alt="Fadhlan" style="width:100%" class="rotateimg180">
          <h3>M Najid Jauhar</h3>
          <p class="w3-opacity">18523272</p>
          <p>Job Desc</p>
        </div>
      </div>
    </div>
</section>

@endsection