@extends('layouts.nav')

@section('content')
<section>
    <div class="card uper">
        <div class="card-header">
            Form Buat Laporan
        </div>
        <div class="card-body">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div> <br />
            @endif
            <form action="/buatlaporan/store" method="post">
                @if(Session::has('success'))
                <div class="alert alert-success">
                    {{Session::get('success')}}
                </div>
                @endif
                @csrf
                <div class="form-group">
                    <label>Nama Pelapor</label>
                    <input type="text" class="form-control" required="required" name="nama">
                    <label>Gambar Cagar Budaya</label> <br>
                    <input type="file" required="required" name="foto"> <br>
                    <label>Deskripsi</label>
                    <textarea name="deskripsi" required="required" cols="180" rows="4"></textarea> <br>
                    <label>Cagar Budaya</label>
                    <select class="form-control" name="cagarbudaya_id" required="required" data-live-search="true" style="width:100%">
                        <option value=""> --Silahkan Pilih-- </option>
                        @foreach ($cagarbudayas as $cb)
                        <option value="{{ $cb->id }}">{{ $cb->nama }}</option>
                        @endforeach
                    </select>

                </div>
                <button type="submit" class="btn btn-primary">Selesai</button>

            </form>
        </div>
    </div>
</section>
@endsection