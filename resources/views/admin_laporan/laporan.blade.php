@extends('layouts.dashboard')

@section('content')

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Laporan Cagar Budaya</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Laporan</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->

<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Tabel Laporan Cagar Budaya</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>User</th>
                                <th>Nama</th>
                                <th>Foto</th>
                                <th>Deskripsi</th>
                                <th>Cagar Budaya</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($laporans as $l)
                            <tr>
                                <td>
                                    {{$l->user['nama']}}
                                </td>
                                <td>
                                    {{$l->nama}}
                                </td>
                                <td>
                                    {{$l->foto}}
                                </td>
                                <td>
                                    {{$l->deskripsi}}
                                </td>
                                <td>
                                    {{$l->cagarbudaya['nama']}}
                                </td>
                                <td>
                                    <a class="btn btn-danger" href="#">Hapus</a>
                                    <a class="btn btn-secondary" href="#">Edit</a></td>
                                </td>

                            </tr>

                            @endforeach
                        </tbody>

                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->


        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

@endsection