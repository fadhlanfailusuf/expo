@extends('layouts.nav')

@section('content')
<section>

    <div class="w3-content">
        <div class="card">
            <div class="card-header">
                <h4>Daftar Jenis Cagar Budaya</h4>
            </div>
            <div class="card-body">
                @foreach($categories as $cat)
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">{{$cat->nama}}</h5>
                        <a href="#" class="btn btn-primary">Lihat {{$cat->nama}}</a>
                    </div>
                </div>
                <br>
                @endforeach
            </div>
        </div>
    </div>
</section>
@endsection