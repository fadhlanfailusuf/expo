@extends('layouts.nav')

@section('content')
<section>

<div class="container">
                <div class="section_title text-center">
                    <h2>Upcoming Events</h2>
                </div>
                <div class="row">
                @foreach($events as $ev)
                    <div class="col-md-4">
                        <div class="event_post">
                            <img src="image/blog1.jpg" alt="">
                            <a href="#"><h2 class="event_title">{{$ev->nama}}</h2></a>
                            <ul class="list_style sermons_category event_category">
                                <li><i class="lnr lnr-user"></i>{{$ev->tanggal_mulai}}</li>
                                <li><i class="lnr lnr-location"></i>{{$ev->cagarbudaya_id}}</li>
                            </ul>
                            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Lihat Detail</button>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>



    @foreach($events as $ev)
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- konten modal-->
            <div class="modal-content">
                <!-- heading modal -->
                <div class="modal-header">
                    <h4 class="modal-title">{{$ev->nama}}</h4>
                </div>
                <!-- body modal -->
                <div class="modal-body">
                    <table class="table table-striped table-responsive-md">
                        <tr>
                            <th>Nama Acara</th>
                            <td>:</td>
                            <td>{{$ev->nama}}</td>
                        </tr>
                        <tr>
                            <th>Tempat Cagar Budaya</th>
                            <td>:</td>
                            <td>@foreach($ev->cagarbudayas as $cb)
                                    {{$cb->nama}}
                                    @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th>Tanggal</th>
                            <td>:</td>
                            <td>{{$ev->tanggal_mulai}} s/d {{$ev->tanggal_selesai}}</td>
                        </tr>
                        <tr>
                            <th>Waktu</th>
                            <td>:</td>
                            <td>{{$ev->waktu_mulai}} s/d {{$ev->waktu_selesai}}</td>
                        </tr>
                        <tr>
                            <th>Deskripsi</th>
                            <td>:</td>
                            <td>{{$ev->deskripsi}}</td>
                        </tr>
                    </table>
                </div>
                <!-- footer modal -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
    @endforeach
    </section>
@endsection