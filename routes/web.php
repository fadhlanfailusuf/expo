<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// use Symfony\Component\Routing\Route;
use Illuminate\Support\Facades\Route;


Route::get('/','HomeController@index');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth', 'checkRole:admin']], function(){

});

Route::group(['middleware' => ['auth', 'checkRole:admin,guest']], function(){

});


Route::get('/admin/jeniscagarbudaya', 'jenisController@index');
Route::get('/admin/jeniscagarbudaya/hapus/{id}', 'jenisController@remove');
Route::get('/admin/jeniscagarbudaya/edit/{id}', 'jenisController@edit');
Route::get('/admin/jeniscagarbudaya/add', 'jenisController@add');
Route::post('/admin/jeniscagarbudaya/store','jenisController@store');
Route::post('/admin/jeniscagarbudaya/update','jenisController@update');

Route::get('/admin/cagarbudaya', 'cagarbudayaController@index');
Route::get('/admin/cagarbudaya/hapus/{id}', 'cagarbudayaController@remove');
Route::get('/admin/cagarbudaya/edit/{id}', 'cagarbudayaController@edit');
Route::get('/admin/cagarbudaya/add', 'cagarbudayaController@add');
Route::post('/admin/cagarbudaya/store','cagarbudayaController@store');
Route::post('/admin/cagarbudaya/update','cagarbudayaController@update');


Route::get('/admin/eventcagarbudaya', 'eventController@index');
Route::get('/admin/eventcagarbudaya/hapus/{id}', 'eventController@remove');
Route::get('/admin/eventcagarbudaya/edit/{id}', 'eventController@edit');
Route::get('/admin/eventcagarbudaya/add', 'eventController@add');
Route::post('/admin/eventcagarbudaya/store','eventController@store');
Route::post('/admin/eventcagarbudaya/update','eventController@update');



Route::get('/admin/laporan', 'laporanController@index');
Route::get('/buatlaporan', 'laporanController@add');
Route::post('/buatlaporan/store', 'laporanController@store');



Route::get('/jeniscagarbudaya', 'HomeController@jenis');
Route::get('/cagarbudaya', 'HomeController@cagarbudaya');
Route::get('/event', 'HomeController@event');
