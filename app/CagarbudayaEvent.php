<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\Console\Helper\Table;

class CagarbudayaEvent extends Model
{
    protected $table = 'cagarbudaya_event';
    protected $fillable = [
        'id', 'cagarbudaya_id', 'event_id',
    ];
}
