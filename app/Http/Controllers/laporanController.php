<?php

namespace App\Http\Controllers;

use App\cagarbudaya;
use App\jeniscb;
use App\event;
use App\laporan;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class laporanController extends Controller
{
    public function index()
    {
        $laporans = laporan::all();
        $users = User::all();
        $cagarbudayas = cagarbudaya::all();
        return view('/admin_laporan/laporan', compact('laporans', 'users', 'cagarbudayas'));
    }

    public function add()
    {
        $users = User::all();
        $cagarbudayas = cagarbudaya::all();
        $jeniscbs = jeniscb::all();
        return view('/admin_laporan/laporan_add', compact('cagarbudayas','jeniscbs'));
    }

    public function store(Request $request)
    {
        DB::table('laporans')->insert([
            'nama' => $request->nama,
            'foto' => $request->foto,
            'deskripsi' => $request->deskripsi,
            'status' => 'belum ditanggapi',
            'cagarbudaya_id' => $request->cagarbudaya_id,
            'user_id' => $request->user_id,      
        ]);
        return redirect('/buatlaporan')->with('success', 'Data berhasil ditambah!');
    }

    public function edit($id)
    {
        // mengambil data berdasarkan id yang dipilih
        $laporans = DB::table('laporans')->where('id', $id)->get();
        // passing data cagarbudaya yang didapat ke view edit.blade.php
        return view('/admin_laporan/laporan_edit', ['laporans' => $laporans]);
    }

    public function update(Request $request)
    {
        // update data cagarbudaya
        DB::table('laporans')->where('id', $request->id)->update([
            'nama' => $request->nama,
            'gambar' => $request->gambar,
            'deskripsi' => $request->deskripsi,
            'cagarbudaya_id' => $request->cagarbudaya_id,
            'user_id' => $request->user_id,    
        ]);
        return redirect('/admin/cagarbudaya');
    }
}
