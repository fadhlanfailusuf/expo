<?php

namespace App\Http\Controllers;

use App\jeniscb;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class jenisController extends Controller
{
    public function index()
    {
        $jeniscbs = jeniscb::all();
        return view('/admin_jenis/jeniscb', compact('jeniscbs'));
    }

    public function add()
    {
        return view('/admin_jenis/jeniscb_add');
    }

    public function store(Request $request)
    {
        DB::table('jeniscbs')->insert([
            'nama' => $request->nama
        ]);
        return redirect('/admin/jeniscagarbudaya')->with('success', 'Data berhasil ditambah!');
    }

    public function remove($id)
    {
        // menghapus data jenis berdasarkan id yang dipilih
        DB::table('jeniscbs')->where('id', $id)->delete();
        // alihkan halaman ke halaman pegawai
        return redirect('/admin/jeniscagarbudaya');
    }

    public function edit($id)
    {
        // mengambil data jenis berdasarkan id yang dipilih
        $jeniscbs = DB::table('jeniscbs')->where('id', $id)->get();
        // passing data jenis yang didapat ke view edit.blade.php
        return view('/admin_jenis/jeniscb_edit', ['jeniscbs' => $jeniscbs]);
    }

    public function update(Request $request)
    {
        // update data pegawai
        DB::table('jeniscbs')->where('id', $request->id)->update([
            'nama'=> $request->nama
        ]);
        return redirect('/admin/jeniscagarbudaya');
    }
}
