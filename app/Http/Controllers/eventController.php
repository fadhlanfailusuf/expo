<?php

namespace App\Http\Controllers;

use App\cagarbudaya;
use App\CagarbudayaEvent;
use App\jeniscb;
use App\event;
use App\laporan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class eventController extends Controller
{
    public function index()
    {
        $events = event::all();
        $cagarbudayas = cagarbudaya::all();
        $jeniscbs = jeniscb::all();
        return view('/admin_event/event', compact('events', 'cagarbudayas', 'jeniscbs'));
    }

    public function remove($id)
    {
        // menghapus data berdasarkan id yang dipilih
        DB::table('events')->where('id', $id)->delete();
        // alihkan halaman ke halaman cagarbudaya
        return redirect('/admin/eventcagarbudaya');
    }

    public function add()
    {
        $events = event::all();
        $cagarbudayas = cagarbudaya::all();
        $jeniscbs = jeniscb::all();
        return view('/admin_event/event_add', compact('events', 'cagarbudayas', 'jeniscbs'));
    }

    public function store(Request $request)
    {   
        $event = new event();
        $event->nama =  $request->nama;
        $event->tanggal_mulai = $request->tanggal_mulai;
        $event->tanggal_selesai = $request->tanggal_selesai;
        $event->waktu_mulai = $request->waktu_mulai;
        $event->waktu_selesai = $request->waktu_selesai;
        $event->deskripsi = $request->deskripsi;
        $event->save();

        $cagarbudayaevent = new CagarbudayaEvent();
        $cagarbudayaevent->cagarbudaya_id = $request->cagarbudaya_id;
        $cagarbudayaevent->event_id = $event->id;
        $cagarbudayaevent->save();

        return redirect('/admin/eventcagarbudaya')->with('success', 'Data berhasil ditambah!');
    }

    public function edit($id)
    {
        // mengambil data berdasarkan id yang dipilih
        $events = DB::table('events')->where('id', $id)->get();
        // passing data yang didapat ke view edit.blade.php
        return view('/admin_event/event_edit', ['events' => $events]);
    }

    public function update(Request $request)
    {
        // update data cagarbudaya
        DB::table('event')->where('id', $request->id)->update([
            'nama' => $request->nama,
            'cagarbudaya_id' => $request->cagarbudaya_id,
            'tanggal_mulai' => $request->tanggal_mulai,
            'tanggal_selesai' => $request->tanggal_selesai,
            'waktu_mulai' => $request->waktu_mulai,
            'waktu_selesai' => $request->waktu_selesai,
            'deskripsi' => $request->deskripsi,

        ]);
        return redirect('/admin/eventcagarbudaya');
    }
}
