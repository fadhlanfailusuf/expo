<?php

namespace App\Http\Controllers;

use App\cagarbudaya;
use App\CagarbudayaEvent;
use App\jeniscb;
use App\event;
use App\laporan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\Input;

class cagarbudayaController extends Controller
{
    public function index()
    {
        $cagarbudayas = cagarbudaya::all();
        $jeniscbs = jeniscb::all();
        return view('/admin_cb/cb', compact('cagarbudayas','jeniscbs'));
    }

    public function remove($id)
    {
        // menghapus data cagarbudaya berdasarkan id yang dipilih
        DB::table('cagarbudayas')->where('id', $id)->delete();
        // alihkan halaman ke halaman cagarbudaya
        return redirect('/admin/cagarbudaya');
    }

    public function add()
    {
        $cagarbudayas = cagarbudaya::all();
        $jeniscbs = jeniscb::all();
        return view('/admin_cb/cb_add', compact('cagarbudayas','jeniscbs'));
    }

    public function store(Request $request)
    {
        // DB::table('cagarbudayas')->insert([
        //     'nama' => $request->nama,
        //     'alamat' => $request->alamat,
        //     'jeniscb_id' => $request->jeniscb_id,
        //     'kota' => $request->kota,
        //     'long_location' => $request->long_location,
        //     'lat_location' => $request->lat_location,
        //     'deskripsi' => $request->deskripsi,
        //     'gambar' => $request->gambar,
        // ]);
        // $cagarbudayas = cagarbudaya::create($request->all());
        $this->validate($request, [
            'nama' => 'required',
            'alamat' => 'required',
            'jeniscb_id' => 'required',
            'kota' => 'required',
            'long_location' => 'required',
            'lat_location' => 'required',
            'deskripsi' => 'required',
            'gambar' => 'required',
        ]);
        $cagarbudayas = new cagarbudaya();
        $cagarbudayas->nama = $request->nama;
        $cagarbudayas->alamat = $request->alamat;
        $cagarbudayas->jeniscb_id = $request->jeniscb_id;
        $cagarbudayas->kota = $request->kota;
        $cagarbudayas->long_location = $request->long_location;
        $cagarbudayas->lat_location = $request->lat_location;
        $cagarbudayas->deskripsi = $request->deskripsi;
        $cagarbudayas->gambar = $request->gambar;
        $cagarbudayas->save();
        
        // if ($request->hasFile('foto')){
        //     $foto = $request->file('foto');
        //     $namafoto = $foto->getClientOriginalName();
        //     $foto->move(\base_path()."/public/image_cagarbudaya", $namafoto);
        //     $save = DB::table('cagarbudayas')->insert(['foto' => $namafoto]);
        //     echo "Berhasil";
        // } else{
        //     echo "upload gagal";
        // }

        // $CagarbudayaEvent = new CagarbudayaEvent();
        // $CagarbudayaEvent->cagarbudaya_id = $cagarbudayas->cagarbudaya_id;
        // $CagarbudayaEvent->event_id = $request->event_id;
        // $CagarbudayaEvent->save();
            
        return redirect('/admin/cagarbudaya')->with('success', 'Data berhasil ditambah!');
    }

    public function edit($id)
    {
        // mengambil data cagarbudaya berdasarkan id yang dipilih
        $cagarbudayas = DB::table('cagarbudayas')->where('id', $id)->get();
        // passing data cagarbudaya yang didapat ke view edit.blade.php
        return view('/admin_cb/cb_edit', ['cagarbudayas' => $cagarbudayas]);
    }

    public function update(Request $request, $id)
    {
        // update data cagarbudaya
        DB::table('cagarbudaya')->where('id', $request->id)->update([
            'nama' => $request->nama,
            'alamat' => $request->alamat,
            'jeniscb_id' => $request->jeniscb_id,
            'kota' => $request->kota,
            'long_location' => $request->long_location,
            'lat_location' => $request->lat_location,
            'deskripsi' => $request->deskripsi,
            'gambar' => $request->gambar,
        ]);
        return redirect('/admin/cagarbudaya');
    }
}
