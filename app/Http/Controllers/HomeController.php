<?php

namespace App\Http\Controllers;

use App\cagarbudaya;
use App\jeniscb;
use App\event;
use App\laporan;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

     // auto login lur
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $cagarbudayas = cagarbudaya::all();
        $jeniscbs = jeniscb::all();
        $events = event::all();
        return view('/home', compact('cagarbudayas','jeniscbs', 'events'));
    }

    public function cagarbudaya()
    {
        $cagarbudayas = cagarbudaya::all();
        return view('cagarbudaya',compact('cagarbudayas'));
    }

    public function jenis()
    {
        $categories = jeniscb::all(); //event dari nama model
        return view('jeniscb',compact('categories'));
    }

    public function event()
    {
        $events = event::all();
        return view('event', compact('events'));
    }

    public function laporan()
    {
        $reports = laporan::all();
        return view('laporan', compact('reports'));
    }

}
