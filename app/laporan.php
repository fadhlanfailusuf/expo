<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\cagarbudaya;
use App\User;

class laporan extends Model
{

    protected $fillable = [
        'id', 'nama', 'foto', 'deskripsi', 'cagarbudaya_id', 'user_id'
    ];

    public function cagarbudayas()
    {
        return $this->hasMany('App\cagarbudaya');
    }

    public function users()
    {
        return $this->hasMany('App\user');
    }
}
