<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\jeniscb;
use App\cagarbudaya;

class event extends Model
{

    protected $fillable = [
        'id', 'nama','tanggal_mulai', 'tanggal_selesai', 'waktu_mulai', 'waktu_selesai', 'keterangan', 'waktu', 'cagarbudaya_id'
    ];

    public function cagarbudayas()
    {
        return $this->belongsToMany('App\cagarbudaya', 'cagarbudaya_event', 'event_id', 'cagarbudaya_id');
    }
}
