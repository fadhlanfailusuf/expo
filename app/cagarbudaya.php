<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\event;
use App\jeniscb;
use App\laporan;

class cagarbudaya extends Model
{

    protected $fillable = [
        'id', 'nama', 'alamat', 'jeniscb_id', 'kota', 'deskripsi', 'gambar', 'lat', 'lng'
    ];

    public function jeniscb()
    {
        return $this->belongsTo('App\jeniscb', 'jeniscb_id');
        // return $this->belongsTo(jeniscb::class);
    }

    public function event()
    {
        return $this->hasMany('App\event');
    }

    public function laporan()
    {
        return $this->belongsToMany('App\laporan')->withTimestamps();
    }


}
