<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jeniscb extends Model
{

    protected $fillable = [
        'id', 'nama'
    ];

    public function cagarbudayas()
    {
        return $this->hasMany('App\cagarbudaya');
        // return $this->hasMany(cagarbudaya::class);
    }
}
